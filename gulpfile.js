// /////////////////////////////////////////////////////
// Required
// /////////////////////////////////////////////////////

var gulp = require('gulp'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	sourcemaps = require('gulp-sourcemaps'),
	plumber = require('gulp-plumber'),
	del = require('del'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload;

// /////////////////////////////////////////////////////
// Scripts Task
// /////////////////////////////////////////////////////

gulp.task('scripts', function() {
	gulp.src(['app/js/**/*.js', '!app/js/**/*.min.js'])
		.pipe(plumber())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(uglify())
		.pipe(gulp.dest('app/js'))
		.pipe(reload({stream:true}));
});


// /////////////////////////////////////////////////////
// Syles Task
// /////////////////////////////////////////////////////


gulp.task('sass', function() {
	gulp.src('app/scss/**/*.scss')
		.pipe(plumber())
		.pipe(sourcemaps.init())
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(sourcemaps.write())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('app/css'))
		.pipe(reload({
			stream: true
		}));
});



// /////////////////////////////////////////////////////
// HTML Task
// /////////////////////////////////////////////////////


gulp.task('html', function() {
	gulp.src('app/**/*.html')
		.pipe(reload({
			stream: true
		}));
});


// /////////////////////////////////////////////////////
// Build Task
// /////////////////////////////////////////////////////

// clear out all files and folders from build folder
gulp.task('build:cleanfolder', function(cb) {
	del([
		'build/**'
	], cb());
});


// task to create build directory for all files
gulp.task('build:copy', ['build:cleanfolder'], function(){
	return gulp.src('app/**/*/')
	.pipe(gulp.dest('build/'));
});


// task to remove unwanted build files
// list all files and directories here that you don't want to include

gulp.task('build:remove', ['build:copy'], function(cb){
	del([
		'build/scss/',
		'build/images/old/',
		// 'build/js/!(*.min.js)'
	], cb);
});


gulp.task('build', ['build:copy', 'build:remove']);

// /////////////////////////////////////////////////////
// Browser-Sync Task
// /////////////////////////////////////////////////////

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: "./app/"
		}
	});

});


// /////////////////////////////////////////////////////
// Watch Task
// /////////////////////////////////////////////////////

gulp.task('watch', function() {
	gulp.watch('app/js/**/*.js', ['scripts']);
	gulp.watch('app/scss/**/*.scss', ['sass']);
	gulp.watch('app/**/*.html', ['html']);
});


// /////////////////////////////////////////////////////
// Default Task
// /////////////////////////////////////////////////////

gulp.task('default', ['scripts', 'sass', 'html', 'browser-sync', 'watch']);
